package com.example.joorebelo.a5314assignment2;

public class PlayersOnFirebase {
    private String deviceId;
    private int score;

    public PlayersOnFirebase(String deviceId, int score) {
        this.deviceId = deviceId;
        this.score = score;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
