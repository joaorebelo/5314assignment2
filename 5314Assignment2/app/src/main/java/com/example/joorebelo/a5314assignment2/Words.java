package com.example.joorebelo.a5314assignment2;

public class Words {
    private String word;
    private boolean isBad;

    public Words(String word, boolean isBad) {
        this.word = word;
        this.isBad = isBad;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public boolean isBad() {
        return isBad;
    }

    public void setBad(boolean bad) {
        isBad = bad;
    }
}