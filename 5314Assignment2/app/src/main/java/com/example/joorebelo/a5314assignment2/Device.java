package com.example.joorebelo.a5314assignment2;

import io.particle.android.sdk.cloud.ParticleDevice;

public class Device {
    private ParticleDevice device;
    private boolean voted;
    private boolean vote;
    private int score;

    public Device(ParticleDevice device) {
        this.device = device;
        this.voted = false;
        this.vote = false;
        this.score = 0;
    }

    public ParticleDevice getDevice() {
        return device;
    }

    public void setDevice(ParticleDevice device) {
        this.device = device;
    }

    public boolean isVoted() {
        return voted;
    }

    public void setVoted(boolean voted) {
        this.voted = voted;
    }

    public boolean isVote() {
        return vote;
    }

    public void setVote(boolean vote) {
        this.vote = vote;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
