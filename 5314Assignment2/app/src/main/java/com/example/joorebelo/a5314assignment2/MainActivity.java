package com.example.joorebelo.a5314assignment2;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import io.particle.android.sdk.cloud.ParticleCloud;
import io.particle.android.sdk.cloud.ParticleCloudSDK;
import io.particle.android.sdk.cloud.ParticleDevice;
import io.particle.android.sdk.cloud.ParticleEvent;
import io.particle.android.sdk.cloud.ParticleEventHandler;
import io.particle.android.sdk.cloud.exceptions.ParticleCloudException;
import io.particle.android.sdk.utils.Async;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("score");

    TextView txtTitle, txtImageName, txtWord, txtTitle2, txtHighScore, txtQuestion;
    Button btnNewGame, btnRestartGame;
    private final String TAG="Joao";
    private int round = 0;
    private int voteCount = 0;
   // private boolean needRestart = false;
    private ArrayList<Words> wordsToShow = new ArrayList<>();
    private Words answer;
    private ArrayList<PlayersOnFirebase> playersOnFirebase = new ArrayList<>();

    //region account
    // MARK: Particle Account Info
    private final String PARTICLE_USERNAME = "joao.rebelo92@gmail.com";
    private final String PARTICLE_PASSWORD = "QWEparticle1";

    // MARK: Particle device-specific info
    private final String DEVICE_ID = "340036000f47363333343437";
    //endregion

    // MARK: Particle Publish / Subscribe variables
    private long subscriptionId;

    // MARK: Particle device
    //private ParticleDevice mDevice;
    private ArrayList<Device> devices = new ArrayList<Device>();;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtTitle = findViewById(R.id.txtTitle);
        txtWord = findViewById(R.id.txtWord);
        txtTitle2 = findViewById(R.id.txtTitle2);
        txtHighScore = findViewById(R.id.txtHighScore);
        txtQuestion = findViewById(R.id.txtQuestion);

        btnNewGame = findViewById(R.id.btnNewGame);
        btnNewGame.setOnClickListener(this);

        btnRestartGame= findViewById(R.id.btnRestartGame);
        btnRestartGame.setOnClickListener(this);

        btnRestartGame.setVisibility(View.INVISIBLE);
        txtWord.setVisibility(View.INVISIBLE);
        txtTitle2.setVisibility(View.VISIBLE);
        txtTitle2.setText("Words");
        txtQuestion.setVisibility(View.INVISIBLE);

        //bad words
        wordsToShow.add(new Words("Porra", true));
        wordsToShow.add(new Words("foda-se", true));
        wordsToShow.add(new Words("Merda", true));
        wordsToShow.add(new Words("Monte de merda", true));
        wordsToShow.add(new Words("Puta que pariu", true));
        //good words
        wordsToShow.add(new Words("Olá", false));
        wordsToShow.add(new Words("Cão", false));
        wordsToShow.add(new Words("Cadeira", false));
        wordsToShow.add(new Words("Adeus", false));
        wordsToShow.add(new Words("Bom dia", false));

        Random randomGenerator = new Random();
        int wordIndex = randomGenerator.nextInt(9);
        answer = new Words(wordsToShow.get(wordIndex).getWord(),wordsToShow.get(wordIndex).isBad());

        txtWord.setText(answer.getWord());

        // Attach a listener to read the data at our posts reference
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Post post = dataSnapshot.getValue(Post.class);
                System.out.println(dataSnapshot);
                Log.e(TAG, "dataSnapshot: " + dataSnapshot);
                playersOnFirebase = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Log.e(TAG, "snapshot id: " + snapshot.getKey() + " value: " + snapshot.getValue());
                    //playersOnFirebase.add(new PlayersOnFirebase(snapshot.getKey(), (Integer) snapshot.getValue()));
                    playersOnFirebase.add(new PlayersOnFirebase(snapshot.getKey(),
                            ((Long) snapshot.getValue()).intValue()));

                    //((Long) snapshot.getValue()).intValue();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        // 1. Initialize your connection to the Particle API
        ParticleCloudSDK.init(this.getApplicationContext());
        // 2. Setup your device variable
        getDeviceFromCloud();


    }

    //Custom function to connect to the Particle Cloud and get the device
    public void getDeviceFromCloud() {
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {

            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                particleCloud.logIn(PARTICLE_USERNAME, PARTICLE_PASSWORD);

                List<ParticleDevice> mDevice = particleCloud.getDevices();

                for (int i = 0; i < mDevice.size();i++){
                    devices.add(new Device(mDevice.get(i)));
                }
                Log.d(TAG, "DNumber: " + devices.size());

                //mDevice = particleCloud.getDevice(DEVICE_ID);
                //particleCloud.getDevices();
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == btnNewGame.getId()){
            subscribeButtonPressed();
            btnNewGame.setVisibility(View.INVISIBLE);
            txtWord.setVisibility(View.VISIBLE);
            txtTitle2.setVisibility(View.VISIBLE);
            txtQuestion.setVisibility(View.VISIBLE);
            txtTitle2.setText("Words(Round " + (round + 1) + ")");
        }else if(v.getId() == btnRestartGame.getId()){
            btnRestartGame.setVisibility(View.INVISIBLE);
            for (int i = 0; i < devices.size(); i++) {
                devices.get(i).setVoted(false);
                devices.get(i).setVote(false);
                devices.get(i).setScore(0);
            }

            voteCount = 0;
            round = 0;

            Random randomGenerator = new Random();
            int wordIndex = randomGenerator.nextInt(9);
            answer = new Words(wordsToShow.get(wordIndex).getWord(), wordsToShow.get(wordIndex).isBad());

            txtWord.setText(answer.getWord());
            txtTitle2.setText("Words(Round " + (round +1) + ")");
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    public void subscribeButtonPressed() {
        Log.d(TAG, "Subscribe button pressed");

        if (devices.size() < 3) {
            Log.d(TAG, "Cannot find 3 devices");
            Toast.makeText(getApplicationContext(), "Cannot find 3 devices!", Toast.LENGTH_SHORT).show();
            return;
        }

        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {
                subscriptionId = ParticleCloudSDK.getCloud().subscribeToAllEvents(
                        "broadcastMessage",  // the first argument, "eventNamePrefix", is optional
                        new ParticleEventHandler() {
                            public void onEvent(String eventName, ParticleEvent event) {

                                Log.i(TAG, "Received event with payload: " + event.dataPayload);
                                Log.i(TAG, "deviceId: " + event.deviceId);
                                verifyGameState(event.dataPayload, event.deviceId);

                            }

                            public void onEventError(Exception e) {
                                Log.e(TAG, "Event error: ", e);
                            }
                        });
                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Successfully got device from Cloud");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });

    }

    private void verifyGameState(String vote, String deviceId) {
        if (vote.equals("score")){
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try { Thread.sleep(1000); }
                    catch (InterruptedException e) {}

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int auxScore = 0;
                            txtHighScore.setText("");
                            String auxScoreTxt = "High Score:";
                            for (int i = 0; i < playersOnFirebase.size();i++){
                                auxScoreTxt += "\n";
                                auxScoreTxt += playersOnFirebase.get(i).getDeviceId() + ": " + playersOnFirebase.get(i).getScore();
                            }
                            txtHighScore.setText(auxScoreTxt);
                        }
                    });
                }
            };
            thread.start();
        }else{
            Log.e(TAG, "verifyGameState");
            for (int i = 0; i < devices.size();i++){
                if (devices.get(i).getDevice().getID().equals(deviceId)){
                    if (!devices.get(i).isVoted()){
                        devices.get(i).setVoted(true);
                        devices.get(i).setVote((vote.equals("true")? true : false));
                        voteCount++;
                    }
                    break;
                }
            }

            if (voteCount == devices.size()){
                Log.d(TAG, "Size: " + devices.size());
                for (int i = 0; i < devices.size();i++){
                    if (devices.get(i).isVote() == answer.isBad()){
                        Log.d(TAG, "isVote: " + devices.get(i).isVote());
                        sendMessage("0,255", devices.get(i).getDevice());
                        devices.get(i).setScore(devices.get(i).getScore()+1);
                    }else{
                        Log.d(TAG, "isVote: " + devices.get(i).isVote());
                        sendMessage("255,0", devices.get(i).getDevice());
                    }
                }
                round++;



                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        try { Thread.sleep(1000); }
                        catch (InterruptedException e) {}

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.e(TAG, ">>>> round: " + round);
                                if (round == 3){
                                    //save score to cloud
                                    Log.e(TAG, "game ended");
                                    int auxScore = 0;
                                    String auxScoreTxt = "Current Game Score:\n";
                                    for (int i = 0; i < devices.size();i++){
                                        if(i != 0){
                                            auxScoreTxt += "\n";
                                        }
                                        if (devices.get(i).getScore() > auxScore){
                                            auxScore = devices.get(i).getScore();
                                        }
                                        auxScoreTxt += devices.get(i).getDevice().getID() + ": " + devices.get(i).getScore();
                                    }
                                    txtHighScore.setText(auxScoreTxt);

                                    for (int i = 0; i < devices.size();i++){
                                        if (devices.get(i).getScore() == auxScore){
                                            String s = myRef.child(devices.get(i).getDevice().getID()).toString();
                                            Log.e(TAG, "firebase: " + s);
                                            boolean foundID = false;

                                            for (int j = 0; j < playersOnFirebase.size();j++){
                                                if (playersOnFirebase.get(j).getDeviceId().equals(devices.get(i).getDevice().getID())){
                                                    foundID = true;
                                                    myRef.child(devices.get(i).getDevice().getID()).setValue((playersOnFirebase.get(j).getScore()+1));
                                                    playersOnFirebase.get(j).setScore((playersOnFirebase.get(j).getScore()+1));
                                                }
                                            }
                                            if (foundID == false){
                                                myRef.child(devices.get(i).getDevice().getID()).setValue(1);
                                                playersOnFirebase.add(new PlayersOnFirebase(devices.get(i).getDevice().getID(),1));
                                            }
                                        }
                                    }


                                    btnRestartGame.setVisibility(View.VISIBLE);
                                }else if (round < 3) {
                                    Log.e(TAG, "clear game");
                                    //clear game
                                    for (int i = 0; i < devices.size(); i++) {
                                        devices.get(i).setVoted(false);
                                        devices.get(i).setVote(false);
                                    }
                                    voteCount = 0;

                                    Random randomGenerator = new Random();
                                    int wordIndex = randomGenerator.nextInt(9);
                                    answer = new Words(wordsToShow.get(wordIndex).getWord(), wordsToShow.get(wordIndex).isBad());

                                    txtWord.setText(answer.getWord());
                                    txtTitle2.setText("Words(Round " + (round +1) + ")");
                                }
                            }
                        });
                    }
                };
                thread.start();
            }
        }



    }

    private void sendMessage(String commandToSend, ParticleDevice device){
        Async.executeAsync(ParticleCloudSDK.getCloud(), new Async.ApiWork<ParticleCloud, Object>() {
            @Override
            public Object callApi(@NonNull ParticleCloud particleCloud) throws ParticleCloudException, IOException {

                List<String> functionParameters = new ArrayList<String>();
                functionParameters.add(commandToSend);

                try {
                    device.callFunction("cLights", functionParameters);
                } catch (ParticleDevice.FunctionDoesNotExistException e) {
                    e.printStackTrace();
                }

                return -1;
            }

            @Override
            public void onSuccess(Object o) {
                Log.d(TAG, "Sent colors command to device.");
            }

            @Override
            public void onFailure(ParticleCloudException exception) {
                Log.d(TAG, exception.getBestMessage());
            }
        });
    }
}
