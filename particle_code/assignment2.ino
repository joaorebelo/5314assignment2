// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
#include "InternetButton.h"
InternetButton b = InternetButton();

void setup() {
    b.begin();
    
    Particle.function("cLights", controlLights);
}

void loop() {
   
    if(b.buttonOn(2)){
        Particle.publish("broadcastMessage", "true", 60, PUBLIC);
        delay(500);
    }
    
    if(b.buttonOn(4)){
        Particle.publish("broadcastMessage", "false", 60, PUBLIC);
        delay(500);
    }
    if(b.buttonOn(3)){
        Particle.publish("broadcastMessage", "score", 60, PUBLIC);
        delay(500);
    }
}

int controlLights(String command){
    
    int cIndex = command.indexOf(",");
    String r = command.substring(0, cIndex);
    Particle.publish("red", r);
    String g = command.substring(cIndex+1);
    
    
    int vR = atoi(r.c_str());
    int vG = atoi(g.c_str());
    int vB = 0;
    
    for(int i = 1; i <= 11; i++) {
        b.ledOn(i, vR, vG, vB);
    }
    
    
    delay(2000);
    b.allLedsOff();
}